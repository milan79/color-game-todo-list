var body = document.querySelector("body");
var squares = document.querySelectorAll(".colorsToGuess");
var displayColorToGuess = document.getElementById("guessColor");
var difficultyButtons = document.getElementsByClassName("difficulty");
var message = document.querySelector("#message");
var h1 = document.getElementsByTagName("h1")[0];
var resetButton = document.querySelector("#reset");

var numberOfColors = 6;
var randomColors = getRandomColors();
var colorToGuess = findColorToGuess();
var opacityChange = false;

for(var i = 0; i < squares.length; i++) {
	squares[i].addEventListener("click", function() {
		var pickedColor = this.style.backgroundColor;
		if(pickedColor === colorToGuess) {
			message.textContent = "Correct!";
			colorSquaresCorrectColor();
			h1.style.backgroundColor = colorToGuess;
			resetButton.textContent = "Play Again?";
		} else {
			message.textContent = "Try Again!";
			this.style.backgroundColor = body.style.backgroundColor;
		}
	});
}

for(var i = 0; i < squares.length; i++) {
	squares[i].style.backgroundColor = randomColors[i];
}

for(var i = 0; i < difficultyButtons.length; i++) {
	difficultyButtons[i].addEventListener("click", function() {
		difficultyButtons[0].classList.remove("selected");
		difficultyButtons[1].classList.remove("selected");
		difficultyButtons[2].classList.remove("selected");
		opacityChange = false;

		this.classList.add("selected");

		if(this.textContent === "Easy") {
			numberOfColors = 3;
			opacityChange = false;
		} else if(this.textContent === "Hard") {
			numberOfColors = 6;
			opacityChange = false;
		} else {
			numberOfColors = 6;
			opacityChange = true;
		}
		reset();
	}); 
}

resetButton.addEventListener("click", function() {
	reset();
});

function reset() {
	randomColors = getRandomColors();
	colorToGuess = findColorToGuess();	
	colorSquares();
	resetButton.textContent = "New Colors";
	h1.style.backgroundColor = "#1d6799";
	message.textContent = "";
}

function colorSquares() {
	for(var i = 0; i < squares.length; i++) {
		if(i < randomColors.length) {
			squares[i].style.backgroundColor = randomColors[i];
		} else {
			squares[i].style.backgroundColor = body.style.backgroundColor;
		}		
	}
}

function colorSquaresCorrectColor() {
	for(var i = 0; i < squares.length; i++) {
		if(i < randomColors.length) {
			squares[i].style.backgroundColor = colorToGuess;
		} else {
			squares[i].style.backgroundColor = body.style.backgroundColor;
		}	
	}
}

function getRandomColors() {
	var colors = [];
	for(var i = 0; i < numberOfColors; i++) {
		colors[i] = generateRandomColor();
	}
	return colors;
}

function generateRandomColor() {
	var color = "";

	var r = Math.floor((Math.random() * 256));
	var g = Math.floor((Math.random() * 256));
	var b = Math.floor((Math.random() * 256));
	var a = 0.7;

	if (opacityChange) {
		color += "rgba(" + r + ", " + g + ", " + b + ", " + a + ")";
	} else {
		color += "rgb(" + r + ", " + g + ", " + b + ")";
	}	

	return color;
}

function findColorToGuess() {
	var index = Math.floor(Math.random() * numberOfColors);
	displayColorToGuess.textContent = randomColors[index];

	return randomColors[index];
}